import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgQuestionComponent } from './svg-question.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgQuestionComponent],
  declarations: [SvgQuestionComponent]
})
export class SvgQuestionModule { }
