import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgWomanCircleComponent } from './svg-woman-circle.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SvgWomanCircleComponent]
})
export class SvgWomanCircleModule { }
