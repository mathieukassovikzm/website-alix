import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgWriteCircleComponent } from './svg-write-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgWriteCircleComponent],
  declarations: [SvgWriteCircleComponent]
})
export class SvgWriteCircleModule { }
