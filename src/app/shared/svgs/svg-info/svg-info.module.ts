import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgInfoComponent } from './svg-info.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgInfoComponent],
  declarations: [SvgInfoComponent]
})
export class SvgInfoModule { }
