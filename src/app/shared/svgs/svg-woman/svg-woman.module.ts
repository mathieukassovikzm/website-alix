import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgWomanComponent } from './svg-woman.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgWomanComponent],
  declarations: [SvgWomanComponent]
})
export class SvgWomanModule { }
