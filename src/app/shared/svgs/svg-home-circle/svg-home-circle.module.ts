import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgHomeCircleComponent } from './svg-home-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgHomeCircleComponent],
  declarations: [SvgHomeCircleComponent]
})
export class SvgHomeCircleModule { }
