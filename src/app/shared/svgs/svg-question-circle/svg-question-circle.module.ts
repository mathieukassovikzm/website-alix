import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgQuestionCircleComponent } from './svg-question-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgQuestionCircleComponent],
  declarations: [SvgQuestionCircleComponent]
})
export class SvgQuestionCircleModule { }
