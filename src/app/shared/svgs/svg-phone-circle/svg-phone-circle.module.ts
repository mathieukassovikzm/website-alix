import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgPhoneCircleComponent } from './svg-phone-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgPhoneCircleComponent],
  declarations: [SvgPhoneCircleComponent]
})
export class SvgPhoneCircleModule { }
