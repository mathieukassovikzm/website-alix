import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgWriteComponent } from './svg-write.component';

@NgModule({
  declarations: [SvgWriteComponent],
  exports: [SvgWriteComponent],
  imports: [CommonModule]
})
export class SvgWriteModule { }
