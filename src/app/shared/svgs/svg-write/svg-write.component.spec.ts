import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SvgWriteComponent } from './svg-write.component';

describe('SvgWriteComponent', () => {
  let component: SvgWriteComponent;
  let fixture: ComponentFixture<SvgWriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SvgWriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgWriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
