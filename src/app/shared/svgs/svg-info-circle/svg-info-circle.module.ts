import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgInfoCircleComponent } from './svg-info-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgInfoCircleComponent],
  declarations: [SvgInfoCircleComponent]
})
export class SvgInfoCircleModule { }
