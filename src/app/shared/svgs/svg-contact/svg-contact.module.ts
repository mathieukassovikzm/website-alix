import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgContactComponent } from './svg-contact.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgContactComponent],
  declarations: [SvgContactComponent]
})
export class SvgContactModule { }
