import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgContactCircleComponent } from './svg-contact-circle.component';

@NgModule({
  imports: [CommonModule],
  exports: [SvgContactCircleComponent],
  declarations: [SvgContactCircleComponent]
})
export class SvgContactCircleModule { }
