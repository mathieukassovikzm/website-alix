import { animation, style, animate, trigger, transition, useAnimation } from '@angular/animations';

export const slideAnimation = trigger('slideInOut', [
  transition(':enter', [
    style({ transform: 'translateY(-100%)' }),
    animate('2000ms ease-in', style({ transform: 'translateY(0%)' }))
  ]),
  transition(':leave', [
    animate('2000ms ease-in', style({ transform: 'translateY(-100%)' }))
  ])
]);