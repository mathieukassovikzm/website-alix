import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderResponsiveComponent } from './header-responsive.component';
import { SvgHomeModule } from '../../svgs/svg-home/svg-home.module';
import { SvgWomanModule } from '../../svgs/svg-woman/svg-woman.module';

@NgModule({
  imports: [
    CommonModule,
    SvgHomeModule,
    SvgWomanModule
  ],
  exports: [HeaderResponsiveComponent],
  declarations: [HeaderResponsiveComponent]
})
export class HeaderResponsiveModule { }
